import React from "react";
import { IoIosMailUnread, IoLogoTwitter, IoLogoInstagram, IoLogoLinkedin } from "react-icons/io";

export default function SocialMedia() {
    return(
        <div className="follow-us">
            <a href="https://www.linkedin.com/in/dpolotsky/"><IoLogoLinkedin/></a>
            <a href="https://twitter.com/dpolotsky"><IoLogoTwitter/></a>
            <a href="https://www.instagram.com/dpolotsky/"><IoLogoInstagram/></a>
            <a href="mailto:contact@coinflip.tech"><IoIosMailUnread/></a>
        </div>
    )
}