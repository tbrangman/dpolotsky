import Head from 'next/head'
import SocialMedia from '../components/social-media.js'
import Particles from 'react-particles-js'

export default function Home() {
  return (
   <>
   <Head>
        <title>Daniel Polotsky - CEO of CoinFlip</title>
        <link rel="shortcut icon" href="/favicon.ico"></link>
        <meta name="viewport" content="initial-scale=1.0, width=device-width" />
        <meta name="description" content="Daniel Polotsky is a FinTech entrepreneur and the CEO and co-founder of CoinFlip ATMs. Daniel founded CoinFlip when he was a sophomore at Northwestern University and grew Coinflip to become the world’s leading Bitcoin ATM operator."></meta>
        <meta name="robots" content="index, follow, max-snippet:-1, max-image-preview:large, max-video-preview:-1"></meta>
        <link rel="canonical" href="https://danielpolotksy.com"></link>
        <meta property="og:locale" content="en_US"></meta>
        <meta property="og:title" content="Daniel Polotsky - CEO of CoinFlip"></meta>
        <meta property="og:description" content="Daniel Polotsky is a FinTech entrepreneur and the CEO and co-founder of CoinFlip ATMs. Daniel founded CoinFlip when he was a sophomore at Northwestern University and grew Coinflip to become the world’s leading Bitcoin ATM operator."></meta>
        <meta property="og:url" content="https://danielpolotksy.com"></meta>
        <meta property="og:site_name" content="Daniel Polotsky"></meta>
        <meta name="twitter:card" content="summary_large_image"></meta>
        <meta name="twitter:creator" content="@dpolotsky"></meta>
        <meta name="twitter:site" content="@dpolotsky"></meta>
        <meta name="twitter:image" content="https://tech-coinflip-www.s3.us-east-2.amazonaws.com/images/cf-header-2020.jpg"></meta>
        {/*

        Made with <3 by @tyvangogh
        
        _______.-.   .-..-.   .-..--.  .-. .-.  ,--,    .---.    ,--,   .-. .-.
        |__   __|\ \_/ )/ \ \ / // /\ \ |  \| |.' .'    / .-. ) .' .'    | | | |
          )| |    \   (_)  \ V // /__\ \|   | ||  |  __ | | |(_)|  |  __ | `-' |
        (_) |     ) (      ) / |  __  || |\  |\  \ ( _)| | | | \  \ ( _)| .-. |
          | |     | |     (_)  | |  |)|| | |)| \  `-) )\ `-' /  \  `-) )| | |)|
          `-'    /(_|          |_|  (_)/(  (_) )\____/  )---'   )\____/ /(  (_)
                (__)                  (__)    (__)     (_)     (__)    (__)    

        */}
    </Head>
    <Particles className="background"
    params={{
	    "particles": {
	        "number": {
	            "value": 25
	        },
	        "size": {
	            "value": 1
          },
          "color": {
            "value": "#232d3c"
          },
          "line_linked": {
            "enable": true,
            "distance": 150,
            "color": {
              "value": "#000000"
            },
            "opacity": 0.4,
            "width": 0.5
          }
	    },
	    "interactivity": {
	        "events": {
	            "onhover": {
	                "enable": true,
	                "mode": "repulse"
	            }
	        }
      },
	}} />
    <div class="description">
        <p>Daniel is the CEO and Co-Founder of CoinFlip, responsible for guiding daily company operations, including managing the company’s rapidly growing network of over 1300+ ATMs and forging business relationships and partnerships. Previously, he was a Global Treasury Operations Analyst for Citadel LLC. In this position, Daniel was in charge of settling Canadian treasury repo and cash trades and recodifying the team’s counterparty payment models. His main skill set includes anticipating cryptocurrency market trends fueled by his knowledge and experience in traditional finance. Daniel earned his Bachelor’s degree in Economics from Northwestern University.</p>
        <SocialMedia/>
    </div>

    <div class="main-nav" href="">
        <p>Daniel</p>
        <p>Polotsky</p>
        <p class="subscript">CEO &amp; Co-Founder of <a href="https://coinflip.tech"><img class="cf-logo" src="/images/cf-logo.svg"/></a></p>
    </div>

    <div class="resume">
        <div class="profile-photo">
            <img src="/images/daniel.webp"/>
        </div>
    </div>
   </>
  )
}
